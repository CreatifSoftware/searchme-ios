//
//  TestProjectTests.swift
//  TestProjectTests
//
//  Created by Kerem Balaban on 7.03.2021.
//

import XCTest
import CoreData
@testable import TestProject

class TestProjectTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testCheckStoreData(urlString:String = "https://bbc.com") {
      // 1. given
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "WebSearch")
        var coreData : [NSManagedObject] = []
        do {
            coreData = try managedContext.fetch(fetchRequest)
            coreData = coreData.filter { (managedObject) -> Bool in
                managedObject.value(forKey: "webPage") as! String == urlString
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }

        XCTAssertTrue(coreData.count > 0)
    }

}
