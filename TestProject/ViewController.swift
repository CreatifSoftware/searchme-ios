//
//  ViewController.swift
//  TestProject
//
//  Created by Kerem Balaban on 7.03.2021.
//

import UIKit
import WebKit
import SwiftSoup
import SVProgressHUD
import CoreData

private let kActivityIndicatorWidth: CGFloat = 100.0
private let kActivityIndicatorHeight: CGFloat = 100.0

class ViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var tvUrlOfWebPage : UITextField!
    @IBOutlet weak var tvExpressionToBeFound : UITextField!
    @IBOutlet weak var searchButton : UIButton!
    @IBOutlet weak var container : UIView!
    var activityIndicator = UIActivityIndicatorView()
    var coreData: [NSManagedObject] = []
    
    // MARK: - Lifecycle's
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prepareTextFields()
        prepareViews()
    }
    
    // MARK: - View helpers
    private func prepareViews(){
        self.view.applyGradient()
        self.searchButton.isEnabled = false
        self.searchButton.alpha = 0.4
    }
    
    private func prepareTextFields(){
        tvUrlOfWebPage.applyBorder(hexString: "#f45c43")
        tvExpressionToBeFound.applyBorder(hexString: "#f45c43")
        
        tvUrlOfWebPage.attributedPlaceholder = NSAttributedString(string: "URL Of Web Page",
                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        tvExpressionToBeFound.attributedPlaceholder = NSAttributedString(string: "Expression To Be Found",
                                                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        tvUrlOfWebPage.setLeftPaddingPoints(16)
        tvExpressionToBeFound.setLeftPaddingPoints(16)
        
        tvUrlOfWebPage.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        tvExpressionToBeFound.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if tvUrlOfWebPage.text!.count > 0 && (tvExpressionToBeFound.text!.count != 0) {
            self.searchButton.isEnabled = true
            self.searchButton.alpha = 1
        }else{
            self.searchButton.isEnabled = false
            self.searchButton.alpha = 0.4
        }
    }
        
    @IBAction func startButtonAction() {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: nil, message: "Searching...", preferredStyle: .alert)
            let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
            loadingIndicator.hidesWhenStopped = true
            loadingIndicator.style = .large
            loadingIndicator.startAnimating();
            
            alert.view.addSubview(loadingIndicator)
            self.present(alert, animated: true) {
                self.searchGivenUrl()
            }
        }
    }
    
    // MARK: - Helper methods
    private func searchGivenUrl(){
        var urlString = self.tvUrlOfWebPage.text!
        if !urlString.hasPrefix("https://") || !urlString.hasPrefix("http://"){
            urlString = "https://\(urlString)"
        }
        
        // check if there is saved data by given url
        let datalist = getContentFromCoreData().filter { (managedObject) -> Bool in
            managedObject.value(forKey: "webPage") as! String == urlString
        }
        var body = ""
        
        //get content of saved data
        if datalist.count > 0 {
            body = datalist.first!.value(forKey: "content") as! String
        }
        
        if !body.isEmpty {
            self.filterInputOnHtml(body,urlString)
        }else{
            guard let url = URL(string: urlString) else {
                self.showAlert("Oppss!","\(urlString) doesn't seem to be a valid URL")
                return
            }
            do {
                let htmlString = try String.init(contentsOf: url)
                let document = try SwiftSoup.parse(htmlString)
                self.filterInputOnHtml(try document.text(),urlString)
            } catch let error {
                self.showAlert("Oppss!","Error: \(error)")
            }
        }
    }
        
    private func filterInputOnHtml(_ htmlString:String, _ urlString:String){
        do {
            saveCoreData(htmlString,urlString)
            let regex = try NSRegularExpression(pattern: "\(tvExpressionToBeFound.text!)", options: .caseInsensitive)
            let matches = regex.matches(in: htmlString, options: [], range: NSRange(location: 0, length: htmlString.utf16.count))
            showAlert("# of hits", "Given expression found \(matches.count) times")
        } catch {
            // regex was bad!
        }
    }
    
    
    private func showAlert(_ title:String, _ message:String) {
        self.dismiss(animated: true) {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: - Core Data
    func saveCoreData(_ htmlString: String, _ urlString: String) {
        guard let appDelegate =
                UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "WebSearch", in: managedContext)!
        
        let webSearch = NSManagedObject(entity: entity,insertInto: managedContext)
        
        webSearch.setValue(htmlString, forKeyPath: "content")
        webSearch.setValue(urlString, forKeyPath: "webPage")
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    private func getContentFromCoreData() -> [NSManagedObject]{
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return []
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "WebSearch")
        
        do {
            coreData = try managedContext.fetch(fetchRequest)
            return coreData
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            return []
        }
    }
}

extension Data {
    func toString() -> String {
        return String(decoding: self, as: UTF8.self)
    }
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
}

extension String {
    func isValidURL () -> Bool {
        let urlRegEx = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?"
        return NSPredicate(format: "SELF MATCHES %@", urlRegEx).evaluate(with: self)
    }
}

extension UIView {
    func applyGradient() {
        let color1 =  UIColor.init(hexString: "#eb3349").cgColor
        let color2 = UIColor.init(hexString: "#f45c43").cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [color2, color1]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1)
        gradientLayer.frame = bounds
        
        layer.insertSublayer(gradientLayer, at:0)
    }
    
    func applyBorder(hexString:String) {
        layer.borderWidth = 1
        layer.borderColor = UIColor.init(hexString: hexString).cgColor
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get{
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
}

extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt64()
        Scanner(string: hex).scanHexInt64(&int)
        let a, r, g, b: UInt64
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

